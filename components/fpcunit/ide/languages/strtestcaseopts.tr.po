msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: tr_TR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2\n"

#: strtestcaseopts.sbtncreate
msgid "Create unit"
msgstr "Birim oluştur"

#: strtestcaseopts.schksetup
msgid "Create Setup Method"
msgstr "Kurulum Yöntemi Oluştur"

#: strtestcaseopts.schktear
msgid "Create TearDown method"
msgstr "TearDown yöntemi oluştur"

#: strtestcaseopts.sfpcunconsoletestapp
msgid "FPCUnit Console Test Application"
msgstr "FPCUnit Konsol Test Uygulaması"

#: strtestcaseopts.sfpcunconsoletestdesc
msgid "An application to run FPCUnit test cases in console mode."
msgstr "FPCUnit test senaryolarını konsol modunda çalıştırmak için bir uygulama."

#: strtestcaseopts.sfpcuntestapp
msgid "FPCUnit Test Application"
msgstr "FPCUnit Test Uygulaması"

#: strtestcaseopts.sfpcuntestappdesc
msgid "An application to run FPCUnit test cases."
msgstr "FPCUnit test senaryolarını çalıştırmak için bir uygulama."

#: strtestcaseopts.sfpcuntestcase
msgid "FPCUnit Test Case"
msgstr "FPCUnit Test Senaryosu"

#: strtestcaseopts.sfpcuntestcasedesc
msgid "A unit containing a FPCUnit Test Case."
msgstr "FPCUnit Test Case içeren bir birim."

#: strtestcaseopts.sfrmtest
msgid "TestCase Options"
msgstr "Test Durumu Seçenekleri"

#: strtestcaseopts.sgrpfixture
msgid "Fixture"
msgstr "Fikstür"

#: strtestcaseopts.sgrpnames
msgid "Names"
msgstr "Adlar"

#: strtestcaseopts.slbldefault
msgid "Default Test Name"
msgstr "Varsayılan Test Adı"

#: strtestcaseopts.swriteyourowntest
msgid "Write your own test"
msgstr "Kendi testini yaz"

